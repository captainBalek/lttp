-- *************************************
-- *                                   *
-- *             Entities              *
-- *                             Class *
-- *************************************
entity = {};function entity:new(o)

	-- properties
	self.id = 0
	self.type = "mob" -- player / npc / mob / boss
	self.x = 2038
	self.y = 1408
	self.spawnX = 2020
	self.spawnY = 1425
	self.hp = 3
	self.speed = 1
	self.state = "idle"
	self.direction = "down"
	self.timer = 0
	self.timerMax = 100
	self.msg_or_ip = ""
	self.port_or_nil = 0

	-- constructor
	local function constructor(o)
		o = o or {}
		setmetatable(o, self)
		self.__index = self		
		return self
	end;self = constructor(o)

	
	-- handle behavior
	function self:update()
		
		-- increment life cycle timer
		self.timer = self.timer + 1

		-- decides action 
		if self.timer >= self.timerMax then
			self.x = math.random(self.spawnX-80,self.spawnX+80)
			self.y = math.random(self.spawnY-25,self.spawnY+25)
			self.timer = 0
			return "action"
		end
		
		return "idle"
		
	end

	
	-- calculate hit from
	function self:hit(x, y)
	
		-- if self within reach of attacker
		if math.abs(self.x - x) < 20 and math.abs(self.y - y) < 20 then			
			self.hp = self.hp - 1
			print("Hit!")
			print("Entity: ", self.id)
			print("HP: ", self.hp)			
		end	

	end
	
	
	-- check if is dead
	function self:isDead()
		if self.hp < 1 then
			return true
		else
			return false
		end
	end
	
	
	return o
	
end