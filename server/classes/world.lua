-- *************************************
-- *                                   *
-- *              World                *
-- *                             Class *
-- *************************************
world = {};function world:new(o)

	-- properties
	self.playerCount = 0
	self.dayNightCycle = 0
	self.bossSpawnTimer = 0
	self.bossSpawnTimerMax = 10000

	-- constructor
	local function constructor(o)
		o = o or {}
		-- nested properties
		o.entities = {}		
		setmetatable(o, self)
		self.__index = self		
		return self
	end;self = constructor(o)

	-- add entity to world
	function self:addEntity(entity)
		
		table.insert(self.entities, entity)
		
	end
	
	-- remove entity from world
	function self:removeEntity(entityId)
	
		local removeBuffer
		local index = 1
	
		for k, v in pairs(self.entities) do
		
			if v.id == entityId then
			
				removeBuffer = index
				break
			
			else
				
				index = index + 1
				
			end
		
		end
		
		for k, v in pairs(self.entities) do
		
			print(v.id)
		
		end
		table.remove(self.entities, removeBuffer)
		
		for k, v in pairs(self.entities) do
		
			print(v.id)
		
		end
	end

	return o
	
end