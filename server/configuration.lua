-- ******************************************
-- *                                        *
-- *          Configuration Object          *
-- *                                        *
-- ******************************************

configuration = {
	updateRate = 10,
	updateTimer = 0,
	persistDataRate = 5000,
	persistTimer = 0,
	mobsUpdateRate = 50,
	mobsUpdateTimer = 0
}

return configuration