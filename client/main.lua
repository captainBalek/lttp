-- *************************************
-- *                                   *
-- *           LTTP online             *
-- *                            Client *
-- *************************************

-- Load function
function love.load()

	-- *************************************
	-- *                                   *
	-- *             Network               *
	-- *                                   *
	-- *************************************
	-- init network configuration
	local socket = require "socket"
	--local address, port = "3.143.215.136", 12345
	local address, port = "127.0.0.1", 12345
	local entity 
	updaterate = 0.1
	t = 0
	udp = socket.udp()
	udp:settimeout(0)
	udp:setpeername(address, port)
	
	-- create unique entity
	math.randomseed(os.time()) 
	playerEntity = tostring(math.random(99999))
	
	-- send connection packet to server
	local dg = string.format("%s %s %d %d", playerEntity, 'at', 1880, 1120)
	udp:send(dg)
	

	-- *************************************
	-- *                                   *
	-- *             Includes              *
	-- *                                   *
	-- *************************************
	require "includes/configuration"
	require "includes/classes/character"
	
	
	-- *************************************
	-- *                                   *
	-- *           World creation          *
	-- *                                   *
	-- *************************************		
	world = {}
	init = 0
	blow = 0
	
	-- *************************************
	-- *                                   *
	-- *          Player creation          *
	-- *                                   *
	-- *************************************		
	player = character:new{entity = playerEntity, type = "player", speed = 1}	
	character:createAnimation()
	table.insert(world, player)		

	ent = 0
end


-- Update function
function love.update(dt)

	-- fps cap
	next_time = next_time + min_dt
	
	-- Count frame
	t = t + dt 
	
	if blow > 0 then
		blow = blow - 1
	end
	
	if player.x > 116 then		
		camera.x = -1*(player.x - 116)
	end

	if player.y > 94 then		
		camera.y = -1*(player.y - 94)
	end
	
	
	-- *************************************
	-- *                                   *
	-- *             Network               *
	-- *                                   *
	-- *************************************	
	-- get server data 
	data, msg = udp:receive()	
	

	if t > updaterate then
		
		-- send player input
		local dg = string.format("%s %s %d %d", playerEntity, 'move', player.x, player.y)
		udp:send(dg)

		-- reinitialize update counter
		t=0
		
	end	
	
	if init == 0 then
		-- send connection packet to server
		local dg = string.format("%s %s %d %d", playerEntity, 'get', 1, 1)
		udp:send(dg)			
		init = 1	
	end
	
	
	-- if server has sent data
	if data then 
	
		-- parse server data 
		ent, cmd, parms = data:match("^(%S*) (%S*) (.*)")
		
		-- create new entity
		if cmd == 'rem' then
			local toRemove = 0
			-- entity position update from client				
			for i=1, #world do						
			  	if world[i].entity == ent then
					toRemove = i			
				end
			end
			table.remove(world, toRemove)
		end		
		
		-- create new entity
		if cmd == 'new' then
		
			-- parse server data
			local x, y = parms:match("^(%-?[%d.e]*) (%-?[%d.e]*)$")	
			
			-- assert server variables for integrity
			assert(x and y)
			
			-- convert from string to number
			x, y = tonumber(x), tonumber(y)		
		
			table.insert(world, character:new{type="ai", entity=ent, x=x, y=y})		
			world[#world]:goTo(x, y)
			character:createAnimation()
		
		end
			
		-- server response to client update query
		if cmd == 'at' then

			-- parse server data
			local x, y = parms:match("^(%-?[%d.e]*) (%-?[%d.e]*)$")	
			
			-- assert server variables for integrity
			assert(x and y)
			
			-- convert from string to number
			x, y = tonumber(x), tonumber(y)
			
			-- entity position update from client				
			for _,v in pairs(world) do						
			  	if v.entity == ent and v.type == "player" then
					--v.x=x 
					--v.y=y					
					break
			  	elseif v.entity == ent and v.type == "ai" then
					v:goTo(x, y)
					break
				end
			end
			
		else
			print("unrecognised command:", cmd)
		end			
	
	elseif msg ~= 'timeout' then 
		error("Network error: "..tostring(msg))
	end


	-- *************************************
	-- *                                   *
	-- *        Entities movement          *
	-- *                                   *
	-- *************************************	
	table.foreach(world, function(k,v) 
		if v.type == "ai" then
			v:aiMove()
		end		
		v:move()
	end)	

end


-- Draw function
function love.draw()


	-- *************************************
	-- *                                   *
	-- *          Screen handling          *
	-- *                                   *
	-- *************************************	

	-- screen resizing
	love.graphics.scale(2,2)
	
	-- *************************************
	-- *                                   *
	-- *       Environement drawing        *
	-- *                                   *
	-- *************************************		
	
	-- maps drawing
	love.graphics.draw(map.img, map.x+camera.x, map.y+camera.y)
	
	
	-- *************************************
	-- *                                   *
	-- *         Entities drawing          *
	-- *                                   *
	-- *************************************	
	table.sort(world, compare)
	table.foreach(world, function(k,v) 


	
		if v.state == "walk" or v.state == "idle" then 
			quad = v.quad.walk[math.floor(v.animation)]
			love.graphics.draw(v.walk, quad, v.x+camera.x, v.y+camera.y)
		end
		if v.state == "attack" then 
			love.graphics.draw(v.attack, quad, v.x+camera.x, v.y+camera.y)
			quad = v.quad.attack[math.floor(v.animation)]
		end
		
		
		

	end)


	-- *************************************
	-- *                                   *
	-- *       Environement drawing        *
	-- *                                   *
	-- *************************************	
	
	-- overlay drawing
	love.graphics.draw(map.entrance, 2016+camera.x, 2128+camera.y)


	-- *************************************
	-- *                                   *
	-- *           HUD drawing             * 
	-- *                                   *
	-- *************************************	

	-- hud drawing
	love.graphics.draw(hud.img, hud.x, hud.y)


	-- *************************************
	-- *                                   *
	-- *              Misc                 *
	-- *                                   *
	-- *************************************	
	
	-- fps counter
	--love.graphics.print(love.timer.getFPS(),220,5)
	--love.graphics.print(tostring(player.x),220,15)
	--love.graphics.print(tostring(player.y),220,25)
	love.graphics.print(player.animation,220,25)	
	
	-- fps cap
	local cur_time = love.timer.getTime()
    if next_time <= cur_time then
       next_time = cur_time
       return
    end
    love.timer.sleep(next_time - cur_time)	
	
end


-- Keypress function
function love.keypressed(button, scancode, isrepeat)	
	if(button == "up") then
		player.input.up = true				
	end
	if(button == "down") then
		player.input.down = true
	end
	if(button == "left") then
		player.input.left = true
	end
	if(button == "right") then
		player.input.right = true
	end
	if(button == "space") and blow == 0 then
		player.animation = 1
		player.input.space = true
		-- send player input
		local dg = string.format("%s %s %d %d", playerEntity, 'att', player.x, player.y)
		udp:send(dg)
		-- cooldown
		blow = 50
	end
end


-- Keyrelease function
function love.keyreleased(button, scancode)
	if(button == "up") then
		player.input.up = false
	end
	if(button == "down") then
		player.input.down = false
	end
	if(button == "left") then
		player.input.left = false
	end
	if(button == "right") then
		player.input.right = false
	end
	if(button == "space") then
		player.input.space = false
	end
end

-- compare function
function compare(a,b)
	return a.y < b.y
end