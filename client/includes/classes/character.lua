-- *************************************
-- *                                   *
-- *             Character             *
-- *                             Class *
-- *************************************
character = {};function character:new(o)

	-- properties
	self.entity = 0
	self.type = "ai"
	self.x = 1880
	self.y = 1120
	self.xDestination = self.x
	self.yDestination = self.y
	self.speed = 1
	self.state = "idle"
	self.direction = "down"
	self.walk = love.graphics.newImage("img/sprites/link-walk.png")
	self.attack = love.graphics.newImage("img/sprites/link-attack.png")
	self.width = 17
	self.height = 24
	self.anim = {
		walk = {
			{
				{51, 68, 85, 102, 85, 68, 51, 34, 17, 0, 17, 34},
				{ 0,  1,  0,   0,  0,  1,  0,  0,  0, 0,  0,  0}
			},
			{
				{51, 68, 85, 102, 85, 68, 51, 34, 17, 0, 17, 34},
				{25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25}
			},
			{
				{51, 68, 85, 102, 51, 34, 17,  0}, 
				{50, 50, 50,  50, 50, 50, 50, 50}
			},
			{
				{51, 68, 85, 102, 51, 34, 17,  0},
				{75, 75, 75,  75, 75, 75, 75, 75}
			}
		},
		attack = {
			{
				{0, 17, 33, 50, 67, 84, 102},
				{0, 0, 0, 0, 0, 0, 0}
			},
			{
				{},
				{}
			},
			{
				{},
				{}
			},
			{
				{},
				{}
			}
		}
	}
	self.quad = {
		walk = {},
		attack = {}
	}
	self.animation = 1
	self.timer = 0
	self.timerMax = 0
	self.attTimerMax = 1000
	self.attTimer = self.attTimerMax	
	self.pathFinding = {}
	o.waypoints = 0

	-- constructor
	local function constructor(o)
		o = o or {}
		-- nested properties
		o.input = {
			up = false,
			down = false,
			left = false,
			right = false,
			space = false
		}		
		o.path = {}
		setmetatable(o, self)
		self.__index = self		
		return self
	end;self = constructor(o)
	
	
	-- create animation quads 
	function self:createAnimation()
		local index = 0	
		for i=1, #self.anim.walk do
			for j=1, #self.anim.walk[i][1] do
				index = index + 1
				self.quad.walk[index] = love.graphics.newQuad(self.anim.walk[i][1][j], self.anim.walk[i][2][j], self.width, self.height, self.walk:getDimensions())
			end
		end
		index = 0
		for i=1, #self.anim.attack do
			for j=1, #self.anim.attack[i][1] do
				index = index + 1
				self.quad.attack[index] = love.graphics.newQuad(self.anim.attack[i][1][j], self.anim.attack[i][2][j], self.width, self.height, self.attack:getDimensions())
			end
		end
	end

	
	-- set destination
	function self:goTo(x, y)
	
		self.xDestination = x
		self.yDestination = y
	
	end
	
	
	-- character movement
	function self:move()
	


		if(self.input.left) then
			self.x = self.x - self.speed
			self.direction = "left"
			self.state = "walk"
		end
		if(self.input.right) then
			self.x = self.x + self.speed
			self.direction = "right"
			self.state = "walk"
		end
		if(self.input.up) then
			self.y = self.y - self.speed
			self.direction = "up"
			self.state = "walk"
		end
		if(self.input.down) then
			self.y = self.y + self.speed
			self.direction = "down"
			self.state = "walk"
		end
		if(self.input.space) then				
			self.state = "attack"
		end		
		
		-- Walk animation
		if self.state == "walk" then
			if self.direction == "down" then
				if self.animation > 12 then
					self.animation = 1
				else				
					self.animation = self.animation + 0.25
				end
			elseif self.direction == "up" then
				if self.animation < 13 or self.animation >= 24 then
					self.animation = 13
				else				
					self.animation = self.animation + 0.25
				end
			elseif self.direction == "right" then
				if self.animation < 25 or self.animation >= 32 then
					self.animation = 25
				else				
					self.animation = self.animation + 0.25
				end
			elseif self.direction == "left" then
				if self.animation < 33 or self.animation >= 40 then
					self.animation = 33
				else				
					self.animation = self.animation + 0.25
				end
			end
		end
		
		-- Attack animation
		if self.state == "attack" then
			self.attTimer = self.attTimer - 1
			if self.attTimer <= 0 then
				self.state = "idle"				
				self.attTimer = self.attTimerMax			
			else
				if self.direction == "down" then
					if self.animation > 7 then
						self.animation = 1						
					else				
						self.animation = self.animation + 0.01
					end
				end
			end
		end
		
		-- Idle animation
		if self.state == "idle" then
			if self.direction == "down" then
				self.animation = 1
			elseif self.direction == "up" then
				self.animation = 13
			elseif self.direction == "right" then
				self.animation = 25
			elseif self.direction == "left" then
				self.animation = 33	
			end
		end
		
		self.state = "idle"
		
	end	
	
	
	-- character movement
	function self:aiMove()	
	
		self.input.down = false;self.input.up = false;self.input.right = false;self.input.left = false
		if self.x < self.xDestination then
			self.input.right = true
		end
		if self.x > self.xDestination then
			self.input.left = true
		end
		if self.y < self.yDestination then
			self.input.down = true
		end
		if self.y > self.yDestination then
			self.input.up = true
		end
		
		if self.input.down == false and
		self.input.up == false and
		self.input.left == false and
		self.input.right == false and
		#self.pathFinding > 0 then
			
			self:goTo(self.pathFinding[1].x, self.pathFinding[1].y)
			table.remove(self.pathFinding, 1)			
			
		end
	
	end

	
	-- npc generate path
	function self:generatePath()			
		
		self.waypoints = math.random(1,20)		
		
		for i=1, self.waypoints do
			table.insert(self.path, {x=math.random(50,180),y=math.random(50,180)})
		end
		
		table.insert(self.path, {x=math.random(50,180),y=100})
		
		self.pathFinding = self.path
		
	end	
	
	return o
	
end