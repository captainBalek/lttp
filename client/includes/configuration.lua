love.graphics.setDefaultFilter('nearest', 'nearest', 0)
success = love.window.setMode(512, 448, {fullscreen=false, resizable=false, vsync=true})
if success == false then
	love.event.quit()
end

map = {}
map.img = love.graphics.newImage("img/tiles/map.png")
map.x = 0
map.y = 0
map.entrance = love.graphics.newImage("img/tiles/castle_entrance.png")
camera = {}
camera.x = 0
camera.y = 0
min_dt = 1/60 
next_time = love.timer.getTime()
love.keyboard.setKeyRepeat( true )

hud = {}
hud.x = 0
hud.y = 0
hud.img = love.graphics.newImage("img/hud/hud.png")