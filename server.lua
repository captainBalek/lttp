-- Display server informations
print "****************************************"
print "*                                      *"
print "*             Test server              *"
print "*                        Serv v0.3     *"
print "*                                      *"
print "****************************************"
print ""
print "Initiating server resources..."

-- Loading resources
socket = require "socket"
config = require("server/configuration")
require("server/classes/session")
require("server/classes/database")
require("server/classes/world")
require("server/classes/entities")
--inputPort = require("server/screens/setup")

-- Loading world
print "Initiating server world..."
world = world:new{}

-- Loading entities
print "Initiating server entities..."

mobCount = 0
for i=0, 2 do
	mobCount = mobCount + 1
	world:addEntity(entity:new{id = (100000+mobCount), timerMax = math.random(5, 10)})
end

for i=0, 2 do
	mobCount = mobCount + 1
	world:addEntity(entity:new{id = (100000+mobCount),  x = 2590, y = 1333, spawnX = 2590, spawnY = 1333,  timerMax = math.random(4, 8)})
end

for i=0, 2 do
	mobCount = mobCount + 1
	world:addEntity(entity:new{id = (100000+mobCount),  x = 2695, y = 1730, spawnX = 2695, spawnY = 1730,  timerMax = math.random(2, 8)})
end

for i=0, 2 do
	mobCount = mobCount + 1
	world:addEntity(entity:new{id = (100000+mobCount),  x = 2745, y = 2050, spawnX = 2745, spawnY = 2050,  timerMax = math.random(1, 5)})
end


for i=0, 6 do
	mobCount = mobCount + 1
	world:addEntity(entity:new{id = (100000+mobCount),  x = 2045, y = 1910, spawnX = 2045, spawnY = 1910,  timerMax = math.random(1, 2)})
end


print "---------------"
print "Server ready..."
print ""

-- Main loop
while running do

	-- *************************************
	-- *                                   *
	-- *         Get clients data          *
	-- *                                   *
	-- *************************************
	data, msg_or_ip, port_or_nil = udp:receivefrom()
	
	-- *************************************
	-- *                                   *
	-- *         Timeout handler           *
	-- *                                   *
	-- *************************************	
	if msg_or_ip ~= 'timeout' then
		--error("Unknown network error: "..tostring(msg))
		--print("error")
	end	

	-- *************************************
	-- *                                   *
	-- *            Main loop              *
	-- *                                   *
	-- *************************************
	if data then
	
		-- parse client response
		id, cmd, parms = data:match("^(%S*) (%S*) (.*)")	
		
		-- *************************************
		-- *                                   *
		-- *             Move cmd              *
		-- *                                   *
		-- *************************************
		if cmd == 'move' then
		
			-- parse client data
			local x, y = parms:match("^(%-?[%d.e]*) (%-?[%d.e]*)$")
			
			-- assert client variables for integrity
			assert(x and y)
			
			-- convert from string to number
			x, y = tonumber(x), tonumber(y)		
		
			-- for each entity in the world
			for k, v in pairs(world.entities) do
			
				-- send information about this entity 
				if v.id == id then
					v.x=x
					v.y=y
				end
			
			end						
			
		end
		
		
		-- *************************************
		-- *                                   *
		-- *              At cmd               *
		-- *                                   *
		-- *************************************
		if cmd == 'at' then
		
			-- parse client data
			local x, y = parms:match("^(%-?[%d.e]*) (%-?[%d.e]*)$")
			
			-- assert client variables for integrity
			assert(x and y)
			
			-- convert from string to number
			x, y = tonumber(x), tonumber(y)
		
			if #world.entities > 0 then		
				
				-- for each entity in the world
				for k, v in pairs(world.entities) do
				
					if v.id ~= id and v.type ~= "mob" then
					
						udp:sendto(string.format("%s %s %d %d", id, 'new', x, y), v.msg_or_ip,  v.port_or_nil)
				
					end
				
				end
			
			end
		
			-- insert entity inside world object
			table.insert(world.entities, entity:new{type="player", id=id, x=x, y=y, msg_or_ip=msg_or_ip, port_or_nil=port_or_nil})
			
			-- Print new entity name
			print("Player connected : ", tostring(id))
			
		end		
		
		
		-- *************************************
		-- *                                   *
		-- *             get cmd               *
		-- *                                   *
		-- *************************************
		if cmd == 'get' then
		
			if #world.entities > 0 then		
				
				-- for each entity in the world
				for k, v in pairs(world.entities) do
				
					-- send information about this entity 
					
					if v.id ~= id then
						udp:sendto(string.format("%s %s %d %d", v.id, 'new', v.x, v.y), msg_or_ip,  port_or_nil)
					end
				
				end
			
			end
			
		end			


		-- *************************************
		-- *                                   *
		-- *            attack cmd             *
		-- *                                   *
		-- *************************************
		if cmd == 'att' then
		
			-- parse client data
			local x, y = parms:match("^(%-?[%d.e]*) (%-?[%d.e]*)$")
			
			-- assert client variables for integrity
			assert(x and y)
			
			-- convert from string to number
			x, y = tonumber(x), tonumber(y)
			
			-- for each entity in the world
			for k, v in pairs(world.entities) do
		
				-- if not attacker
				if v.id ~= id then
		
					-- calculate hit
					v:hit(x, y)
					
					-- if dead					
					if v:isDead() then
						
						print("Dead entity: ", v.id)
						
						-- remove entity from world
						world:removeEntity(v.id)
						
						-- update client world state
						for pk, pv in pairs(world.entities) do
							
							if pv.type == "player" then			
					
								udp:sendto(string.format("%s %s %d %d", v.id, 'rem', v.x, v.y), pv.msg_or_ip,  pv.port_or_nil)
								
							end
							
						end
						
					end
			
				end
			
			end		
		
		end				
		
		
		-- *************************************
		-- *                                   *
		-- *             Quit cmd              *
		-- *                                   *
		-- *************************************
		if cmd == 'quit' then
			running = false;		
		end
		
		
		-- *************************************
		-- *                                   *
		-- *           Unknown cmd             *
		-- *                                   *
		-- *************************************		
		if cmd ~= 'quit' and cmd ~= 'at' and cmd ~= 'update' and cmd ~= 'move' and cmd ~= 'get' and cmd ~= 'new' and cmd ~= 'att' then
			print("unrecognised command:", cmd)
		end
		
		
	end

	-- *************************************
	-- *                                   *
	-- *           Ai handling             *
	-- *                                   *
	-- *************************************	
	
	-- for each entity in the world 
	if config.mobsUpdateTimer > config.mobsUpdateRate then
		
		for k, v in pairs(world.entities) do
			
			-- if mob
			if v.type == "mob" then
				
				-- update behavior				
				if v:update() == "action" then
				
					-- for each entity in the world
					for pk, pv in pairs(world.entities) do
					
						if pv.type == "player" then			
				
							udp:sendto(string.format("%s %s %d %d", v.id, 'at', v.x, v.y), pv.msg_or_ip,  pv.port_or_nil)													
							
						end
						
					end
					
				end
				
			end

		end		
		
		config.mobsUpdateTimer = 0
	
	end

	config.mobsUpdateTimer = config.mobsUpdateTimer + 1

	-- *************************************
	-- *                                   *
	-- *             Update cmd            *
	-- *                                   *
	-- *************************************
	
	-- 
	if config.updateTimer > config.updateRate and #world.entities > 0 then
	
		-- for each entity in the world
		for k, v in pairs(world.entities) do
		
			if v.type == "player" then

				-- send information about this entity 
				for i=1, #world.entities do

					if world.entities[i].type == "player" then

						udp:sendto(string.format("%s %s %d %d", world.entities[i].id, 'at', world.entities[i].x, world.entities[i].y), v.msg_or_ip,  v.port_or_nil)

					end

				end
				
			end
		
		end
		
		config.updateTimer = 0
		
	end
	
	config.updateTimer = config.updateTimer + 1
	
	
	-- *************************************
	-- *                                   *
	-- *       Server refresh rate         *
	-- *                                   *
	-- *************************************
	socket.sleep(0.01)
	
end	